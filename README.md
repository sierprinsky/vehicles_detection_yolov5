## <div align="center">Vehicles-OpenImages > 416x416</div>
https://public.roboflow.ai/object-detection/vehicles-openimages

Provided by [Jacob Solawetz](https://roboflow.ai)
License: CC BY 4.0

![Image example](https://i.imgur.com/ztezlER.png)

## <div align="center">Overview</div>

This dataset contains 627 images of various vehicle classes for object detection. These images are derived from the [Open Images](https://arxiv.org/pdf/1811.00982.pdf) open source computer vision datasets.

This dataset only scratches the surface of the Open Images dataset for vehicles!

![Image example](https://i.imgur.com/4ZHN8kk.png)

## <div align="center">Use Cases</div>

* Train object detector to differentiate between a car, bus, motorcycle, ambulance, and truck.
* Checkpoint object detector for autonomous vehicle detector
* Test object detector on high density of ambulances in vehicles
* Train ambulance detector
* Explore the quality and range of Open Image dataset

## <div align="center">Tools Used to Derive Dataset</div>

These images were gathered via the [OIDv4 Toolkit](https://github.com/EscVM/OIDv4_ToolKit) This toolkit allows you to pick an object class and retrieve a set number of images from that class **with bound box lables**. 

We provide this dataset as an example of the ability to query the OID for a given subdomain. This dataset can easily be scaled up - please reach out to us if that interests you. 

-------------------------------------------------------------------------------------------------------
## <div align="center">YOLOv5 Documentation</div>

See the [YOLOv5 Docs](https://docs.ultralytics.com) for full documentation on training, testing and deployment.

## <div align="center">Quick Start Examples</div>

<details open>
<summary>Install</summary>

Clone repo and install [requirements.txt](https://github.com/ultralytics/yolov5/blob/master/requirements.txt) in a
[**Python>=3.7.0**](https://www.python.org/) environment, including
[**PyTorch>=1.7**](https://pytorch.org/get-started/locally/).

```bash
git clone https://github.com/ultralytics/yolov5  # clone
cd yolov5
pip install -r requirements.txt  # install
```

</details>

<details open>
<summary>Inference</summary>

Inference with YOLOv5 and [PyTorch Hub](https://github.com/ultralytics/yolov5/issues/36)
. [Models](https://github.com/ultralytics/yolov5/tree/master/models) download automatically from the latest
YOLOv5 [release](https://github.com/ultralytics/yolov5/releases).

```python
import torch

# Model
model = torch.hub.load('ultralytics/yolov5', 'yolov5s')  # or yolov5m, yolov5l, yolov5x, custom

# Images
img = 'https://ultralytics.com/images/zidane.jpg'  # or file, Path, PIL, OpenCV, numpy, list

# Inference
results = model(img)

# Results
results.print()  # or .show(), .save(), .crop(), .pandas(), etc.
```

</details>



<details>
<summary>Inference with detect.py</summary>

`detect.py` runs inference on a variety of sources, downloading [models](https://github.com/ultralytics/yolov5/tree/master/models) automatically from
the latest YOLOv5 [release](https://github.com/ultralytics/yolov5/releases) and saving results to `runs/detect`.

```bash
python detect.py --source 0  # webcam
                          img.jpg  # image
                          vid.mp4  # video
                          path/  # directory
                          path/*.jpg  # glob
                          'https://youtu.be/Zgi9g1ksQHc'  # YouTube
                          'rtsp://example.com/media.mp4'  # RTSP, RTMP, HTTP stream
```

</details>

<details>
<summary>Training</summary>

The commands below reproduce YOLOv5 [COCO](https://github.com/ultralytics/yolov5/blob/master/data/scripts/get_coco.sh)
results. [Models](https://github.com/ultralytics/yolov5/tree/master/models)
and [datasets](https://github.com/ultralytics/yolov5/tree/master/data) download automatically from the latest
YOLOv5 [release](https://github.com/ultralytics/yolov5/releases). Training times for YOLOv5n/s/m/l/x are
1/2/4/6/8 days on a V100 GPU ([Multi-GPU](https://github.com/ultralytics/yolov5/issues/475) times faster). Use the
largest `--batch-size` possible, or pass `--batch-size -1` for
YOLOv5 [AutoBatch](https://github.com/ultralytics/yolov5/pull/5092). Batch sizes shown for V100-16GB.

```bash
python train.py --data coco.yaml --cfg yolov5n.yaml --weights '' --batch-size 128
                                       yolov5s                                64
                                       yolov5m                                40
                                       yolov5l                                24
                                       yolov5x                                16
```

</details>



## <div align="center">Thanks to:</div>
https://roboflow.ai

https://ultralytics.com/

https://github.com/ultralytics/yolov5

---------------------------------------------------------------------------------------------------------

## <div align="center">Training results summary</div>


<details>
<summary>YOLOv5, nano model:</summary>

```bash
50 epochs completed in 2.965 hours.
Optimizer stripped from runs/train/exp/weights/last.pt, 3.8MB
Optimizer stripped from runs/train/exp/weights/best.pt, 3.8MB

Validating runs/train/exp/weights/best.pt...
Fusing layers... 
Model summary: 213 layers, 1765930 parameters, 0 gradients, 4.2 GFLOPs
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 25/25 [00:14<00:00,  1.69it/s]                                                       
                 all        250        454      0.665      0.577      0.588      0.417
           Ambulance        250         64      0.742       0.75      0.847      0.661
                 Bus        250         46       0.69      0.696      0.654      0.519
                 Car        250        238      0.646      0.445      0.517      0.334
          Motorcycle        250         46      0.695      0.696      0.606      0.344
               Truck        250         60       0.55        0.3      0.318      0.228
Results saved to runs/train/exp
```
</details>
 

<details>
<summary>YOLOv5, small model:</summary>

```bash
50 epochs completed in 6.808 hours.
Optimizer stripped from runs/train/exp2/weights/last.pt, 14.4MB
Optimizer stripped from runs/train/exp2/weights/best.pt, 14.4MB

Validating runs/train/exp2/weights/best.pt...
Fusing layers... 
Model summary: 213 layers, 7023610 parameters, 0 gradients, 15.8 GFLOPs
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 25/25 [00:33<00:00,  1.34s/it]                                                       
                 all        250        454       0.68      0.617      0.619      0.436
           Ambulance        250         64      0.808      0.844      0.824      0.692
                 Bus        250         46      0.666      0.696      0.716       0.55
                 Car        250        238      0.677      0.429      0.533      0.343
          Motorcycle        250         46      0.692      0.783       0.63       0.37
               Truck        250         60      0.555      0.333      0.391      0.223
Results saved to runs/train/exp2

```
</details>


<details>
<summary>YOLOv5, medium model:</summary>

```bash
50 epochs completed in 18.449 hours.
Optimizer stripped from runs/train/exp3/weights/last.pt, 42.1MB
Optimizer stripped from runs/train/exp3/weights/best.pt, 42.1MB

Validating runs/train/exp3/weights/best.pt...
Fusing layers... 
Model summary: 290 layers, 20869098 parameters, 0 gradients, 48.0 GFLOPs
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 25/25 [02:28<00:00,  5.96s/it]                                                       
                 all        250        454      0.696      0.566      0.652      0.496
           Ambulance        250         64      0.826      0.812      0.907      0.775
                 Bus        250         46      0.823      0.696      0.803      0.647
                 Car        250        238      0.637      0.458      0.552      0.366
          Motorcycle        250         46      0.704      0.565      0.646      0.444
               Truck        250         60      0.492        0.3       0.35      0.247
Results saved to runs/train/exp3

```
</details>


<details>
<summary>YOLOv5, large model:</summary>

```bash
50 epochs completed in 29.218 hours.
Optimizer stripped from runs/train/exp4/weights/last.pt, 92.8MB
Optimizer stripped from runs/train/exp4/weights/best.pt, 92.8MB

Validating runs/train/exp4/weights/best.pt...
Fusing layers... 
Model summary: 367 layers, 46129818 parameters, 0 gradients, 107.9 GFLOPs
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 63/63 [02:12<00:00,  2.11s/it]                                                       
                 all        250        454       0.64      0.594      0.626       0.47
           Ambulance        250         64      0.801      0.756      0.817      0.694
                 Bus        250         46      0.581      0.662      0.723      0.585
                 Car        250        238      0.614      0.482      0.551      0.371
          Motorcycle        250         46      0.794      0.739      0.682      0.422
               Truck        250         60      0.409      0.333      0.359      0.278
Results saved to runs/train/exp4


```
</details>

--------------------------------------------------------------------------------------------------------


## <div align="center">Validation results summary</div>

<details>
<summary>YOLOv5, nano model:</summary>

```bash
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 8/8 [00:08<00:00,  1.02s/it]                                                         
                 all        250        454      0.675      0.577      0.592      0.422
           Ambulance        250         64      0.742       0.75      0.852      0.665
                 Bus        250         46       0.69      0.696       0.66      0.526
                 Car        250        238      0.659      0.445      0.519      0.336
          Motorcycle        250         46      0.696      0.696      0.609      0.347
               Truck        250         60      0.586        0.3      0.322      0.233
Speed: 0.7ms pre-process, 28.9ms inference, 0.6ms NMS per image at shape (32, 3, 416, 416)
Results saved to runs/val/exp

```
</details>
 

<details>
<summary>YOLOv5, small model:</summary>

```bash
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 8/8 [00:16<00:00,  2.06s/it]                                                         
                 all        250        454      0.679      0.617       0.62      0.439
           Ambulance        250         64      0.808      0.844      0.827      0.694
                 Bus        250         46      0.666      0.696      0.709      0.553
                 Car        250        238      0.677      0.429      0.544      0.351
          Motorcycle        250         46      0.692      0.783      0.633      0.373
               Truck        250         60      0.554      0.333      0.386      0.223
Speed: 0.6ms pre-process, 62.6ms inference, 0.5ms NMS per image at shape (32, 3, 416, 416)
Results saved to runs/val/exp2

```
</details>


<details>
<summary>YOLOv5, medium model:</summary>

```bash
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 8/8 [00:44<00:00,  5.62s/it]                                                         
                 all        250        454      0.701      0.566      0.653      0.498
           Ambulance        250         64      0.825      0.812      0.909      0.776
                 Bus        250         46      0.823      0.696      0.801      0.649
                 Car        250        238       0.66      0.457      0.562      0.373
          Motorcycle        250         46      0.704      0.565      0.641      0.444
               Truck        250         60      0.492        0.3      0.354       0.25
Speed: 0.7ms pre-process, 176.4ms inference, 0.5ms NMS per image at shape (32, 3, 416, 416)
Results saved to runs/val/exp3

```
</details>


<details>
<summary>YOLOv5, large model:</summary>

```bash
               Class     Images     Labels          P          R     mAP@.5 mAP@.5:.95: 100%|██████████| 8/8 [01:39<00:00, 12.44s/it]                                                         
                 all        250        454      0.648      0.595      0.624      0.473
           Ambulance        250         64      0.829      0.757      0.821      0.696
                 Bus        250         46      0.581      0.662      0.714      0.588
                 Car        250        238      0.627      0.481      0.557      0.378
          Motorcycle        250         46      0.794      0.739      0.667      0.422
               Truck        250         60      0.409      0.333       0.36      0.279
Speed: 0.8ms pre-process, 394.4ms inference, 0.6ms NMS per image at shape (32, 3, 416, 416)
Results saved to runs/val/exp4

```
</details>

-------------------------------------------------------------------------------------------------------

## <div align="center">Validation confusion matrix</div>


<summary>YOLOv5, nano model:</summary>
<p align="left"><img width="600" src="Yolov5/runs/val/exp/confusion_matrix.png"></p>


 
<summary>YOLOv5, small model:</summary>
<p align="left"><img width="600" src="Yolov5/runs/val/exp2/confusion_matrix.png"></p>


<summary>YOLOv5, medium model:</summary>
<p align="left"><img width="600" src="Yolov5/runs/val/exp3/confusion_matrix.png"></p>


<summary>YOLOv5, large model:</summary>
<p align="left"><img width="600" src="Yolov5/runs/val/exp4/confusion_matrix.png"></p>


## <div align="center">Detection examples</div>

<p align="left"><img width="600" src="Yolov5/runs/detect/exp4/00dea1edf14f09ab_jpg.rf.3f17c8790a68659d03b1939a59ccda80.jpg"></p>


<p align="left"><img width="600" src="Yolov5/runs/detect/exp4/259ff749ac781352_jpg.rf.8acc4aba3916d2dd58c3acca8890194b.jpg"></p>
 
